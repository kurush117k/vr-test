﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCannon : MonoBehaviour
{
    #region AutoCannon Variables

    #region AutoCannon Variables Serialized

    [SerializeField]
    protected Transform m_muzzle;
    [SerializeField]
    protected AudioSource m_fireAudio;
    [SerializeField]
    protected AudioSource m_hitAudio;
    [SerializeField]
    protected AudioSource m_coolDownAudio;
    [SerializeField]
    protected ParticleSystem m_muzzleFlashEffect;
    [SerializeField]
    protected ParticleSystem m_bulletHitEffect;
    [SerializeField]
    protected ParticleSystem m_coolDownEffect;
    [SerializeField]
    protected ParticleSystem m_bulletTracerEffect;

    #endregion

    #region AutoCannon Variables Genreal

    #region Float

    protected float m_fireRate;
    protected float m_fireTimer;
    protected float m_heatCollected;
    protected float m_heatPercent;
    protected float m_heatPerShoot;
    protected float m_maxCoolDownTime;
    protected float m_maxRange;

    #endregion

    #region Int

    protected int m_damage;
    protected int m_maxArraySize;

    #endregion

    #region Bool

    protected bool m_overheated;
    protected bool m_cooling;
    protected bool m_inRange;

    #endregion

    #region Raycast

    protected LayerMask m_cannonMask;

    protected RaycastHit m_cannonHit;
    protected Ray m_cannonTrajectory;

    protected Ray m_targetRay;
    protected RaycastHit m_targetHit;

    #endregion

    #region Particle Systems

    protected ParticleSystem[] m_muzzleFlashEffectArray;
    protected ParticleSystem[] m_bulletHitEffectArray;

    #endregion

    #endregion

    #endregion

    // Use this for initialization
    void Start ()
    {
        Initialisation();
        EffectsPool();
	}

    #region AutoCannon Initialisation

    public virtual void Initialisation ()
    {
        m_fireRate = 10.0f;
        m_fireTimer = 1.0f;
        m_heatCollected = 0.0f;
        m_heatPercent = 100.0f;
        m_heatPerShoot = 2.5f;
        m_maxCoolDownTime = 10.0f;
        m_maxRange = 500.0f;

        m_damage = 25;
        m_maxArraySize = (int)(m_heatPercent / m_heatPercent);

        m_cannonMask = 1 << 9;

        m_overheated = false;
        m_cooling = false;
    }

    protected virtual void  EffectsPool ()
    {
        if (m_muzzleFlashEffect)
        {
            m_muzzleFlashEffectArray = new ParticleSystem[m_maxArraySize];

            for (int i = 0; i < m_maxArraySize; i++)
            {
                m_muzzleFlashEffectArray[i] = Instantiate(m_muzzleFlashEffect) as ParticleSystem;
                m_muzzleFlashEffectArray[i].gameObject.SetActive(false);
            }
        }

        if (m_bulletHitEffect)
        {
            m_bulletHitEffectArray = new ParticleSystem[m_maxArraySize];

            for (int i = 0; i < m_maxArraySize; i++)
            {
                m_bulletHitEffectArray[i] = Instantiate(m_bulletHitEffect) as ParticleSystem;
                m_bulletHitEffectArray[i].gameObject.SetActive(false);
            }
        }
    }

    #endregion

    // Update is called once per frame
    void Update()
    {
        AutoCannonOverHeat();
        TargetInRange();
    }

    #region AutoCannon Updates

    public virtual void AutoCannonFire ()
    {
        if (m_overheated == false)
        {
            if (m_fireTimer < -5)
            {
                m_fireTimer = -5;
            }

            if (Time.timeScale != 0.0f)
            {
                if (m_fireTimer <= 0)
                {
                    DetectHit();
                    m_heatCollected += m_heatPerShoot;

                    if (m_muzzleFlashEffect)
                    {
                        for (int i = 0; i < m_maxArraySize; i++)
                        {
                            if (m_muzzleFlashEffectArray[i].gameObject.activeSelf == false)
                            {
                                m_muzzleFlashEffectArray[i].gameObject.SetActive(true);
                                m_muzzleFlashEffectArray[i].transform.position = m_muzzle.position;
                                m_muzzleFlashEffectArray[i].transform.rotation = m_muzzle.rotation;
                                m_muzzleFlashEffectArray[i].Play();
                                break;
                            }
                        }
                    }

                    if (m_bulletTracerEffect)
                    {
                        m_bulletTracerEffect.transform.position = m_muzzle.position;
                        m_bulletTracerEffect.Play();
                    }

                    if (m_fireAudio)
                    {
                        m_fireAudio.Play();
                    }

                    if (m_cooling == false)
                    {
                        StartCoroutine(AutoCool());
                    }

                    m_fireTimer = 1;
                }
            }
        }

        m_fireTimer -= Time.deltaTime * m_fireRate;
    }

    protected virtual void AutoCannonOverHeat ()
    {
        if (m_heatCollected >= m_heatPercent && m_overheated == false)
        {
            m_overheated = true;

            if (m_coolDownAudio)
            {
                m_coolDownAudio.Play();
            }

            if (m_coolDownEffect) 
            {
                m_coolDownEffect.Play();
            }

            StartCoroutine(Cooldown());
        }
    }

    protected virtual void TargetInRange ()
    {
        m_targetRay = new Ray(m_muzzle.position, m_muzzle.forward);

        if (Physics.Raycast(m_targetRay, out m_targetHit, m_maxRange))
        {
            if (m_targetHit.collider.name == "Shield")
            {
                m_inRange = true;
            }
            else
            {
                m_inRange = false;
            }
        }
        else
        {
            m_inRange = false;
        }
    }

    protected virtual void DetectHit ()
    {
        GameObject m_objectHit;

        m_cannonTrajectory = new Ray(m_muzzle.position, m_muzzle.forward);

        if (Physics.Raycast(m_cannonTrajectory, out m_cannonHit, m_maxRange))
        {
            if (m_cannonHit.collider.name == "Shield") 
            {
                m_objectHit = m_cannonHit.collider.gameObject;

                m_objectHit.GetComponent<GateShield>().ShieldDamage(m_damage);
            }

            if (m_bulletHitEffectArray != null)
            {
                for (int i = 0; i < m_maxArraySize; i++)
                {
                    if (m_bulletHitEffectArray[i].gameObject.activeSelf == false)
                    {
                        m_bulletHitEffectArray[i].gameObject.SetActive(true);
                        m_bulletHitEffectArray[i].transform.position = m_cannonHit.point;
                        m_bulletHitEffectArray[i].transform.rotation = Quaternion.identity;
                        m_bulletHitEffectArray[i].Play();

                        if (m_hitAudio)
                        {
                            m_hitAudio.Play();
                        }

                        break;
                    }
                }
            }
        }
    }

    #endregion

    #region AutoCannon Coroutine

    protected IEnumerator Cooldown ()
    {
        float m_time;

        m_time = 0.0f;

        while (m_heatCollected > 0)
        {
            m_time += Time.deltaTime;

            if (m_time >= (m_maxCoolDownTime / m_heatPercent))
            {
                m_heatCollected -= 1;

                if (m_heatCollected < 0)
                {
                    m_heatCollected = 0;
                }

                m_time = 0.0f;
            }

            yield return null;
        }

        m_time = 0.0f;
        m_overheated = false;
    }

    protected IEnumerator AutoCool ()
    {
        float m_time;

        m_cooling = true;
        m_time = 0.0f;

        while (m_heatCollected > 0)
        {
            m_time += Time.deltaTime;

            if (m_time> 1.0f)
            {
                if (m_overheated == false)
                {
                    m_heatCollected -= 2;

                    if (m_heatCollected < 0)
                    {
                        m_heatCollected = 0;
                    }

                    m_time = 0.0f;
                }
            }

            yield return null;
        }

        m_time = 0.0f;
        m_cooling = false;
    }

    #endregion

    #region AutoCannon Get and Set

    public bool InRange
    {
        get
        {
            return m_inRange;
        }
    }

    public float HeatCollected
    {
        get
        {
            return m_heatCollected;
        }
    }

    #endregion
}
