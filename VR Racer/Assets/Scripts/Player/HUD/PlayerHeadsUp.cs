﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerHeadsUp : MonoBehaviour
{
    protected GameObject m_hud;
    protected GameObject m_paused;
    protected GameObject m_gameover;
    protected GameObject m_respawn;

    protected Text m_healthText;
    protected Text m_livesText;
    protected Text m_speedText;
    protected Text m_gatesText;
    protected Text m_heatText;

    protected Camera m_camera;

    [SerializeField]
    protected RawImage m_target;

    protected PlayerRacer m_player;
    protected AutoCannon m_autoCannon;
    protected GateControl m_gateController;
    protected GameController m_gameController;
    protected EventSystem m_event;

	// Use this for initialization
	void Start ()
    {
        Initialisation();
	}

    public virtual void Initialisation ()
    {
        m_hud = GameObject.Find("HUD");
        m_paused = GameObject.Find("Pause");
        m_gameover = GameObject.Find("GameOver");
        m_respawn = GameObject.Find("Respawn");

        m_player = GameObject.Find("DronePlayer").GetComponent<PlayerRacer>();
        m_autoCannon = GameObject.Find("DronePlayer").GetComponent<AutoCannon>();
        m_gateController = GameObject.Find("GateController").GetComponent<GateControl>();
        m_gameController = GameObject.Find("GameController").GetComponent<GameController>();
        m_event = GameObject.Find("EventSystem").GetComponent<EventSystem>();

        m_healthText = GameObject.Find("HealthHUD").GetComponent<Text>();
        m_livesText = GameObject.Find("LivesHUD").GetComponent<Text>();
        m_speedText = GameObject.Find("SpeedHUD").GetComponent<Text>();
        m_gatesText = GameObject.Find("GateHUD").GetComponent<Text>();
        m_heatText = GameObject.Find("HeatHUD").GetComponent<Text>();

        m_camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update ()
    {
        CheckGameState();
        UpdateHud();
        UpdateTarget();
    }

    protected virtual void CheckGameState ()
    {
        if (m_gameController.GamePaused == false && m_gameController.GameOver == false && m_player.RacerHealth > 0)
        {
            m_hud.SetActive(true);
            m_paused.SetActive(false);
            m_gameover.SetActive(false);
            m_respawn.SetActive(false);
        }
        else if (m_gameController.GamePaused == true && m_gameController.GameOver == false && m_player.RacerHealth > 0)
        {
            m_hud.SetActive(false);
            m_paused.SetActive(true);
            m_gameover.SetActive(false);
            m_respawn.SetActive(false);

            m_event.firstSelectedGameObject = GameObject.Find("QuitPauseButton");
        }
        else if ((m_gameController.GamePaused == true || m_gameController.GamePaused == false) && m_gameController.GameOver == true && m_player.RacerHealth <= 0) 
        {
            m_hud.SetActive(false);
            m_paused.SetActive(false);
            m_gameover.SetActive(true);
            m_respawn.SetActive(false);

            m_event.firstSelectedGameObject = GameObject.Find("RetryButton");
        }
        else if ((m_gameController.GamePaused == true || m_gameController.GamePaused == false) && m_gameController.GameOver == false && m_player.RacerHealth <= 0) 
        {
            m_hud.SetActive(false);
            m_paused.SetActive(false);
            m_gameover.SetActive(false);
            m_respawn.SetActive(true);

            m_event.firstSelectedGameObject = GameObject.Find("RespawnButton");
        }
    }

    protected virtual void UpdateHud ()
    {
        if (m_healthText != null)
        {
            m_healthText.text = "Health : " + m_player.RacerHealth;
        }

        if (m_livesText != null)
        {
            m_livesText.text = "Lives : " + m_player.RacerLives;
        }

        if (m_speedText != null) 
        {
            m_speedText.text = "Speed : " + m_player.DisplaySpeed;
        }

        if (m_heatText != null)
        {
            m_heatText.text = "Heat : " + m_autoCannon.HeatCollected;
        }

        if(m_gatesText != null)
        {
            m_gatesText.text = "Gates Cleared : " + m_gateController.GatesPased + "/" + m_gateController.TotalGates;
        }
    }

    protected virtual void UpdateTarget ()
    {
        if (m_autoCannon.InRange == true) 
        {
            m_target.color = Color.red;
        }
        else 
        {
            m_target.color = Color.green;
        }
    }
}
