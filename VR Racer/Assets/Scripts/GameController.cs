﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]
    protected string m_nextLevel;

    protected float m_gameDefualtTime;

    protected bool m_gamePaused;
    protected bool m_gameOver;

    protected PlayerRacer m_player;
    protected GameObject m_playerObject;
    protected GateControl m_gateControl;
    protected Transform m_spawn;
    protected Camera m_levelCamera;

	// Use this for initialization
	void Start ()
    {
        Initialisation();
    }
	
    public virtual void Initialisation ()
    {
        m_gameDefualtTime = Time.timeScale;

        m_gamePaused = false;
        m_gameOver = false;

        m_playerObject = GameObject.Find("DronePlayer");
        m_player = m_playerObject.GetComponent<PlayerRacer>();
        m_gateControl = GameObject.Find("GateController").GetComponent<GateControl>();
        m_spawn = GameObject.Find("Spawn").GetComponent<Transform>();

        m_levelCamera = GameObject.Find("LevelCamera").GetComponent<Camera>();
        m_levelCamera.enabled = false;
    }

    // Update is called once per frame
    void Update ()
    {
        CheckGates();
        CheckGameOver();
        CheckGamePause();
        ChangeCamera();

        Respawn();
        Retry();
    }

    protected virtual void CheckGates ()
    {
        if (m_gateControl.GatesCleared == true)
        {
            SceneManager.LoadScene(m_nextLevel);
        }
    }

    protected virtual void CheckGameOver ()
    {
        if (m_player.RacerLives <= 0)
        {
            m_gameOver = true;
        }
    }

    protected virtual void CheckGamePause ()
    {
        if (Input.GetButtonDown("Pause")) 
        {
            if (m_gamePaused == false)
            {
                m_gamePaused = true;
                Time.timeScale = 0.0f;
            }
            else if (m_gamePaused == true)
            {
                m_gamePaused = false;
                Time.timeScale = m_gameDefualtTime;
            }
        }
    }

    protected virtual void ChangeCamera ()
    {
        if (m_playerObject.activeSelf == false)
        {
            m_levelCamera.enabled = true;
        }
        else if (m_playerObject.activeSelf == true)
        {
            m_levelCamera.enabled = false;
        }
    }

    protected virtual void Respawn ()
    {
        if (m_player.RacerLives > 0)
        {
            if (m_player.RacerHealth <= 0)
            {
                if (Input.GetButtonDown("Fire"))
                {
                    m_player.Initialisation();
                    m_player.InitialisationCamera();
                    m_playerObject.transform.position = m_spawn.transform.position;
                    m_playerObject.transform.rotation = m_spawn.transform.rotation;
                    m_playerObject.SetActive(true);
                }
            }
        }
    }

    protected virtual void Retry ()
    {
        if (m_player.RacerLives <= 0)
        {
            if (Input.GetButtonDown("Fire"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    public virtual void MainMenu ()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public virtual void QuitGame ()
    {
        Application.Quit();
    }

    public bool GamePaused
    {
        get
        {
            return m_gamePaused;
        }
    }

    public bool GameOver
    {
        get
        {
            return m_gameOver;
        }
    }
}
