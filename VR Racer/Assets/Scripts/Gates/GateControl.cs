﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GateControl : MonoBehaviour
{
    protected int m_gatesPassed;

    protected bool m_gatesCleared;

    GameObject[] m_gates;
    GameObject m_currentGate;

	// Use this for initialization
	void Start ()
    {
        Initialisation();
	}

    public virtual void Initialisation ()
    {
        m_gates = GameObject.FindGameObjectsWithTag("Gate");
        m_currentGate = m_gates[0];
        m_currentGate.GetComponent<Gate>().Active = true;

        m_gatesPassed = 0;
        m_gatesCleared = false;
    }

    // Update is called once per frame
    void Update ()
    {
        NextGate();
        CheckGates();
	}

    protected virtual void NextGate ()
    {
        //Gate m_tempGate = m_currentGate.GetComponent<Gate>();

        if (m_currentGate.GetComponent<Gate>().Deactivated == true)
        {
            if (m_gatesPassed < m_gates.Length)
            {
                m_gatesPassed += 1;
            }

            foreach (GameObject m_gate in m_gates)
            {
                if (m_gate.GetComponent<Gate>().Active == false)
                {
                    m_currentGate = m_gate;
                    m_currentGate.GetComponent<Gate>().Active = true;
                    break;
                }
            }
        }
    }

    protected virtual void CheckGates ()
    {
        if (m_gatesPassed >= m_gates.Length)
        {
            m_gatesCleared = true;
        }
    }

    public int TotalGates
    {
        get
        {
            return m_gates.Length;
        }
    }

    public int GatesPased
    {
        get
        {
            return m_gatesPassed;
        }
    }

    public bool GatesCleared
    {
        get
        {
            return m_gatesCleared;
        }
    }
}
