﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]

public class Gate : MonoBehaviour
{
    enum GateAudioType {Defualt, Active, Deactivated};
    #region Gate Variables

    protected bool m_active;
    protected bool m_deactivated;

    protected Light m_statusLight;

    protected AudioSource m_gateDefualtAudio;
    protected AudioSource m_gateActiveAudio;
    protected AudioSource m_gateDeactivatedAudio;

    #endregion

    // Use this for initialization
    void Start ()
    {
        Initialisation();
	}

    #region Gate Initialisation

    public virtual void Initialisation ()
    {
        m_active = false;
        m_deactivated = false;

        m_statusLight = gameObject.GetComponent<Light>();
        m_statusLight.type = LightType.Point;
        m_statusLight.range = 20;
        m_statusLight.intensity = 20;

        this.GetComponent<BoxCollider>().isTrigger = true;
    }

    protected virtual void InitialisationAudio ()
    {
        AudioSource[] m_gateAudio = this.gameObject.GetComponents<AudioSource>();

        for (int i = 0; i > m_gateAudio.Length; i++)
        {
            if (i == (int)GateAudioType.Defualt)
            {
                m_gateDefualtAudio = m_gateAudio[i];
                m_gateDefualtAudio.loop = true;
                m_gateDefualtAudio.Play();
            }

            if (i == (int)GateAudioType.Active)
            {
                m_gateActiveAudio = m_gateAudio[i];
                m_gateActiveAudio.loop = false;
            }
            
            if (i == (int)GateAudioType.Deactivated) 
            {
                m_gateDeactivatedAudio = m_gateAudio[i];
                m_gateDeactivatedAudio.loop = false;
            }
        }
}

    #endregion

    // Update is called once per frame
    void Update ()
    {
        StatusIndicator();
	}

    #region Gate Updates

    protected virtual void StatusIndicator ()
    {
        if (m_statusLight != null)
        {
            if (m_active == false && m_deactivated == false)
            {
                m_statusLight.color = Color.red;
            }
            else if (m_active == true && m_deactivated == false)
            {
                m_statusLight.color = Color.green;

                if (m_gateActiveAudio != null) 
                {
                    m_gateActiveAudio.Play();
                }
            }
            else if (m_active == true && m_deactivated == true)
            {
                m_statusLight.color = Color.blue;
            }
        }
    }

    #endregion

    #region Gate Triggers

    void OnTriggerEnter(Collider m_collider)
    {
        if (m_active == true)
        {
            if (m_collider.gameObject.name == "DronePlayer") 
            {
                m_deactivated = true;
            }
        }
    }

    #endregion

    #region AutoCannon Get and Set

    public bool Deactivated
    {
        get
        {
            return m_deactivated;
        }
    }

    public bool Active
    {
        get
        {
            return m_active;
        }
        set
        {
            m_active = value;
        }
    }

    #endregion
}
