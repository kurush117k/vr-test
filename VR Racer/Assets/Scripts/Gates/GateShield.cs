﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateShield : MonoBehaviour
{
    protected int m_shieldStrength;

    protected float m_shieldAlpha;

    protected AudioSource m_shieldDropAudio;
    protected Animator m_shieldAnim;
    protected Renderer m_shieldRender;

	// Use this for initialization
	void Start ()
    {
        Initialisation();
        SetRenderer();
	}

    public virtual void Initialisation()
    {
        m_shieldStrength = 100;

        m_shieldAlpha = 0.5f;

        m_shieldDropAudio = gameObject.GetComponent<AudioSource>();
        m_shieldAnim = gameObject.GetComponent<Animator>();
        m_shieldAnim.SetBool("ShieldDrop", false);
    }

    protected virtual void SetRenderer()
    {
        m_shieldRender = gameObject.GetComponent<Renderer>();

        Color m_color;

        m_color = m_shieldRender.materials[0].color;
        m_color.a = m_shieldAlpha;

        m_shieldRender.materials[0].color = m_color;
    }

    // Update is called once per frame
    void Update ()
    {
        CheckShieldStrength();
	}

    public virtual void ShieldDamage (int m_damage)
    {
        m_shieldStrength -= m_damage;
    }

    protected virtual void CheckShieldStrength()
    {
        if (m_shieldStrength <= 0)
        {
            StartCoroutine(ShieldDrop());
        }
    }

    protected virtual IEnumerator ShieldDrop()
    {
        if (m_shieldDropAudio != null)
        {
            m_shieldDropAudio.Play();
        }

        m_shieldAnim.SetBool("ShieldDrop", true);

        yield return new WaitForSeconds(m_shieldAnim.GetCurrentAnimatorClipInfo(0).Length);

        gameObject.SetActive(false);
    }
}
