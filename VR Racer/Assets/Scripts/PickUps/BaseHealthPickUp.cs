﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealthPickUp : MonoBehaviour
{
    protected int m_healthRestored;

	// Use this for initialization
	void Start ()
    {
        Initialisation();
	}

    public virtual void Initialisation ()
    {
        m_healthRestored = 20;
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    void OnCollisionEnter(Collision m_collision)
    {
        gameObject.SetActive(false);
    }

    public int HealthRestord
    {
        get
        {
            return m_healthRestored;
        }
    }
}
